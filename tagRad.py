#!/usr/bin/env python
# coding: utf-8

# In[55]:


import uproot, json, os, math, multiprocessing
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from utils import *
import scipy as sy
from scipy.optimize import curve_fit
from scipy.integrate import quad
#%matplotlib widget


# In[56]:


# Set particle type, up to now only e+ and e- can be chosen
particle = 'e-'

#Parameters
l_depth_fix=14.6 #um
BeamEnergy= 855E6 #eV
BE= str(int(BeamEnergy/1e6)) #MeV
BendingAngle = 467 #urad
hkl = (1,1,1)
U_0_111=19
R = l_depth_fix /BendingAngle *10**10 #Ang--->my value
print(R*1e-10)
d_pl = [2.35, 0.78]
coll_angle = 40/8627


# In[57]:

# Path for data
path = "./Bent/467/test100" 
# Opening functions in utils.py
# Rad data
df_rad = get_rad_data(path+f'/radfiles_{BE}', apply_collimation= True, coll_angle=coll_angle)
df_rad_filtered = df_rad.loc[df_rad['spectra'] == 1]
print(df_rad_filtered)


# Deflection data (IF IT IS USEFUL)
df_defl = get_defl_data(path+f'/radfiles_{BE}')
#print(df_defl)

# DATA FOR TAG
df_txt = open_txt_file(f'{path}/tagfiles_{BE}')
#print(df_txt)
#print(df_txt[df_txt['eventID']==16722])

#print(df_txt_filtered['eventID'].unique())
# In[57]:
#[16722, 22139, 42314, 49988, 58680, 60456, 62533, 73085, 90722]
########################## PLOT BEFORE ADJUSTING THE TRAJECTORY
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15, 10))
#j=73085
j= 90722
# Primo subplot
ax1.plot(df_txt['z'].loc[df_txt["eventID"]==j], df_txt['x_v'].loc[df_txt["eventID"]==j], color='blue')#, s=5)
ax1.set_xlabel('z[$\AA$]')
ax1.set_ylabel('x_v[$\AA$]')
ax1.grid(True)

# Secondo subplot
ax2.plot(df_txt['z'].loc[df_txt["eventID"]==j], df_txt['xx'].loc[df_txt["eventID"]==j], color='#00ff00', linewidth=0.8)
ax2.set_xlabel('z')
ax2.set_ylabel('xx')
ax2.grid(True)

# Terzo subplot
ax3.scatter(df_txt['z'].loc[df_txt["eventID"]==j], df_txt['tx'].loc[df_txt["eventID"]==j]*10**6, color='#1cdfc4', s=5)
ax3.set_xlabel('z[$\AA$]')
ax3.set_ylabel('tx[urad]')
ax3.grid(True)

# Sesto subplot (aggiunto vuoto per completare la griglia)
ax4.axis('off')  # Disattiva il sesto subplot se non necessario

# Mostra il plot
plt.tight_layout()
#plt.savefig(f"{path}/{BE}/trajectories_and_other_stuff-{BE}.png", dpi=300)
plt.show()


#print(df_rad)
# get the ids of the particle that emitted radiation
rad_ids = df_rad['eventID'].unique().tolist()
print(rad_ids)



# filter the other dataframes to select only these particles
df_defl_filtered = df_defl[df_defl['eventID'].isin(rad_ids)].copy()
#print(df_defl_filtered)
df_txt_filtered = df_txt[df_txt['eventID'].isin(rad_ids)].copy()
#print(df_txt_filtered)

####### old code
# dataframes = []
# for j in rad_ids:
#     eventID_list = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'eventID'].values
#     x_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'x_v'].values
#     tx_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'tx'].values
#     z_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'z'].values
#     xx_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'xx'].values

#     #print(z_val)
    
#     for i in range(len(z_val) - 1):
#         if (z_val[i+1]<z_val[i]):
#             index_inversion_coordinate = i+1        
#             print(i+1)
#             print(z_val[i+1])
#     #print(index_inversion_coordinate)
#     emission_index = np.abs(z_val[index_inversion_coordinate]-z_val[:index_inversion_coordinate-1]).argmin()
#     #print(emission_index)
#     new_eventID = np.concatenate([eventID_list[:emission_index], eventID_list[index_inversion_coordinate:]])
#     new_x_val = np.concatenate([x_val[:emission_index], x_val[index_inversion_coordinate:]])
#     new_tx_val = np.concatenate([tx_val[:emission_index], tx_val[index_inversion_coordinate:]])
#     new_z_val = np.concatenate([z_val[:emission_index], z_val[index_inversion_coordinate:]])
#     new_xx_val = np.concatenate([xx_val[:emission_index], xx_val[index_inversion_coordinate:]])

#     new_df_txt_filtered = pd.DataFrame({
#         'eventID': new_eventID,
#         'x_v': new_x_val,
#         'tx': new_tx_val,
#         'z': new_z_val,
#         'xx': new_xx_val
#     })
#     dataframes.append(new_df_txt_filtered)
# final_df_txt = pd.concat(dataframes, ignore_index=True)
#print(final_df_txt)
####### new code

dataframes = []
for j in rad_ids:

    eventID_list = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'eventID'].values
    x_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'x_v'].values
    tx_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'tx'].values
    z_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'z'].values
    #print(z_val)
    xx_val = df_txt_filtered.loc[df_txt_filtered['eventID'] == j, 'xx'].values



    #print(z_val)
    inds_inv_coord = []
    for i in range(len(z_val) - 1):
        if (z_val[i+1]<z_val[i]):
            inds_inv_coord.append(i+1)        
            #print(i+1)
            #print(z_val[i+1])
    #print('inversion indexes: ',inds_inv_coord)

    emission_idxs = []
    for ii in inds_inv_coord:
    #    print(ii)
    #print(index_inversion_coordinate)
        emission_idxs.append(np.abs(z_val[ii]-z_val[:ii]).argmin())
    couples=np.stack([inds_inv_coord, emission_idxs],axis=1)
    #print(couples)
    #print(couples[0])
    #print(emission_idxs)

    if len(inds_inv_coord)==1:
        new_eventID = np.concatenate([eventID_list[:couples[0][1]], eventID_list[couples[0][0]:]])
        new_x_val = np.concatenate([x_val[:couples[0][1]], x_val[couples[0][0]:]])
        new_tx_val = np.concatenate([tx_val[:couples[0][1]], tx_val[couples[0][0]:]])
        new_z_val = np.concatenate([z_val[:couples[0][1]], z_val[couples[0][0]:]])
        new_xx_val = np.concatenate([xx_val[:couples[0][1]], xx_val[couples[0][0]:]])

    if len(inds_inv_coord)==2:
        new_eventID = np.concatenate([eventID_list[:couples[0][1]], eventID_list[couples[0][0]:couples[1][0]]])
        new_x_val = np.concatenate([x_val[:couples[0][1]], x_val[couples[0][0]:couples[1][0]]])
        new_tx_val = np.concatenate([tx_val[:couples[0][1]], tx_val[couples[0][0]:couples[1][0]]])
        new_z_val = np.concatenate([z_val[:couples[0][1]], z_val[couples[0][0]:couples[1][0]]])
        new_xx_val = np.concatenate([xx_val[:couples[0][1]], xx_val[couples[0][0]:couples[1][0]]])

    if len(inds_inv_coord)==3:

        new_eventID = np.concatenate([eventID_list[:couples[0][1]], eventID_list[couples[0][0]:couples[1][1]], eventID_list[couples[1][1]:couples[2][1]]])
        new_x_val = np.concatenate([x_val[:couples[0][1]], x_val[couples[0][0]:couples[1][1]], x_val[couples[1][1]:couples[2][1]]])
        new_tx_val = np.concatenate([tx_val[:couples[0][1]], tx_val[couples[0][0]:couples[1][1]], tx_val[couples[1][1]:couples[2][1]]])
        new_z_val = np.concatenate([z_val[:couples[0][1]], z_val[couples[0][0]:couples[1][1]], z_val[couples[1][1]:couples[2][1]]])
        new_xx_val = np.concatenate([xx_val[:couples[0][1]], xx_val[couples[0][0]:couples[1][1]], xx_val[couples[1][1]:couples[2][1]]])

    new_df_txt_filtered = pd.DataFrame({
        'eventID': new_eventID,
        'x_v': new_x_val,
        'tx': new_tx_val,
        'z': new_z_val,
        'xx': new_xx_val
        })
    dataframes.append(new_df_txt_filtered)
final_df_txt = pd.concat(dataframes, ignore_index=True)

#print(emission_index)
#for iii in range(len(inds_inv_coord)):

#new_eventID = np.concatenate([eventID_list[:emission_index], eventID_list[index_inversion_coordinate:]])
#    new_x_val = np.concatenate([x_val[:couples[iii][1]], x_val[couples[iii][0]:]])
#new_tx_val = np.concatenate([tx_val[:emission_index], tx_val[index_inversion_coordinate:]])
#    new_z_val = np.concatenate([z_val[:couples[iii][1]], z_val[couples[iii][0]:]])
#print(new_z_val)
#new_xx_val = np.concatenate([xx_val[:emission_index], xx_val[index_inversion_coordinate:]])

#     new_df_txt_filtered = pd.DataFrame({
#         'eventID': new_eventID,
#         'x_v': new_x_val,
#         'tx': new_tx_val,
#         'z': new_z_val,
#         'xx': new_xx_val
#     })
#     dataframes.append(new_df_txt_filtered)
# final_df_txt = pd.concat(dataframes, ignore_index=True)
# print(final_df_txt)
fig, ax = plt.subplots(figsize=(10, 6))
#j=73085
# Primo subplot
ax.plot(new_z_val, new_x_val, color='blue')#, s=5)
ax.set_xlabel('z[$\AA$]')
ax.set_ylabel('x_v[$\AA$]')
ax.grid(True)


# In[57]:

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(15, 10))
j=73085
# Primo subplot
ax1.plot(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['x_v'].loc[final_df_txt["eventID"]==j], color='blue')#, s=5)
ax1.set_xlabel('z[$\AA$]')
ax1.set_ylabel('x_v[$\AA$]')
ax1.grid(True)

# Secondo subplot
ax2.plot(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['xx'].loc[final_df_txt["eventID"]==j], color='#00ff00', linewidth=0.8)
ax2.set_xlabel('z')
ax2.set_ylabel('xx')
ax2.grid(True)

# Terzo subplot
ax3.scatter(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['tx'].loc[final_df_txt["eventID"]==j]*10**6, color='#1cdfc4', s=5)
ax3.set_xlabel('z[$\AA$]')
ax3.set_ylabel('tx[urad]')
ax3.grid(True)

# Quarto subplot (aggiunto vuoto per completare la griglia)
ax4.axis('off')  # Disattiva il quarto subplot se non necessario

# Mostra il plot
#plt.tight_layout()
#plt.savefig(f"{path}/{BE}/trajectories_and_other_stuff-{BE}.png", dpi=300)
#plt.show()


# In[58]:
#                                                   Calculate the potential U_eff

z1 = 14  # Nuclear charge Si
aTF = 0.8853 * 0.52917721067 * z1**(-1/3)  # Angstrom Tomas-Fermi radius

z2 = -1 if particle == 'e-' else 1# Particle charge
alpha = 1/137.03599907444
alattice = 5.4307  # Angs lattice constant
N0 = 8/alattice**3

me = 0.510998928e6  # eV mass of electron
u1 = 0.075  # Angs amplitude of oscillations

hbarc = 197.32697182500785e1  # eV*Angs
dL = alattice * np.sqrt(3)/4
dS = dL/3
d0 = dS*2
d1 = 2*d0
d2 = d1 - dL/2

Alpha = np.array([1.876, 2.617, 0.8604, 0.3903, 0.07769])
Beta = np.array([62.11, 18.68, 3.935, 0.7690, 0.08234])/(4*np.pi**2)
V0 = 2 * np.sqrt(np.pi) * N0 * d0 * z2 * hbarc**2 / me

x = np.linspace(-5, 5, 10000)
#xx = np.linspace(-2*d1, 2*d1, 10000)

V0at_alpha_alpha = (2 / np.sqrt(np.pi)) * z2 * hbarc**2 / me * Alpha
def UplDT(xx):
    sum_result = 0
    for i in range(5):
        exponent = -(xx**2) / (Beta[i] + 2 * u1**2)
        denominator = np.sqrt(Beta[i] + 2 * u1**2)
        sum_result += V0 * Alpha[i] * np.exp(exponent) / denominator
    return sum_result

def U1plDT02(xx):
    sum_result = 0
    for i in range(-10, 10):  # i va da -15 a 15
        sum_result += UplDT(xx + i*d1 - d2) + UplDT(xx + dS + i*d1 - d2)
    return sum_result

# Compute UDT values
UDT = np.array([U1plDT02(xxx) for xxx in x])
print(np.shape(UDT))

# Find minimum and maximum potential energy values
minU = np.min(UDT)
maxU = np.max(UDT)
print("Minimum potential energy:", minU)
print("Maximum potential energy:", maxU)
print("Range of potential energy:", maxU - minU)
UDT = np.array([udt-minU for udt in UDT])
print(np.shape(UDT))
R_new = R
U_eff_DT = np.array([udt1+BeamEnergy/R_new*x_prime for udt1, x_prime in zip(UDT, x)])
print(np.shape(U_eff_DT))

# Plot UDT
fig, ax1 = plt.subplots(figsize=[7, 5])
ax1.plot(x, U_eff_DT, label="Effective Potential", lw =2)  

ax1.set_xlabel('x, $\AA$', fontsize=16) 
ax1.set_ylabel('Potential Energy, eV', fontsize=16)  
ax1.tick_params(axis='both', which='major', labelsize=16)  
ax1.legend(fontsize=16)

ax1.spines['top'].set_linewidth(1.5)
ax1.spines['right'].set_linewidth(1.5)
ax1.spines['left'].set_linewidth(1.5)
ax1.spines['bottom'].set_linewidth(1.5)

ax1.xaxis.set_tick_params(width=1.5)
ax1.yaxis.set_tick_params(width=1.5)


ax1.grid(False)
plt.show()


#plt.savefig(f'{path}/{BE}/Potential_{BE}.png')


# In[57]:

parallel = False
if parallel:

    N_part = 10
    df_parts = np.array_split(df_txt_filtered, N_part)

    # Calcolo parallelo di E_T
    E_T_parts = parallelize_dataframe_parts(df_parts, calculate_E_T, x)

    # Unisci i risultati di E_T
    df_txt_filtered['E_T'] = np.concatenate(E_T_parts)
    # Calcolo parallelo di E_K
    E_K_parts = parallelize_dataframe_parts(df_parts, calculate_E_K)

    # Unisci i risultati di E_K
    df_txt_filtered['E_K'] = np.concatenate(E_K_parts)
    # Calcolo parallelo degli stati
    states_parts = parallelize_dataframe_parts(df_parts, determine_states, x)
    # Unisci i risultati degli stati
    df_txt_filtered['state'] = np.concatenate(states_parts)

# In[61]:
x_values = final_df_txt['x_v'].to_numpy() #Angstrom
theta_values = final_df_txt['tx'].to_numpy() #rad  
E_K = BeamEnergy/2 * theta_values**2

len_df = len(final_df_txt)
print(len_df)
E_T = np.zeros(len_df, dtype=np.double)
state = np.empty(len_df, dtype="str")
if particle == 'e+':
    for j in range(len_df):
        i = np.abs(x - x_values[j]).argmin()
        E_T[j] = E_K[j] + U_eff_DT[i]
        toll = 150
        if hkl == (1,1,1): #gpaterno
            if ((x_values[j] >= 0) and (x_values[j] <= d_pl[0]/2)):
                if E_T[j] <= max(U_eff_DT[np.abs(-(d_pl[0]/2)-x).argmin()-toll:np.abs(-(d_pl[0]/2)-x).argmin()+toll]):
                    state[j] = 'c'
                else:
                    state[j] = 'o'
            elif ((x_values[j] >= d_pl[0]/2) and (x_values[j] <= d_pl[0]/2+d_pl[1])):
                if E_T[j] <= max(U_eff_DT[np.abs(d_pl[0]/2-x).argmin()-toll:np.abs(d_pl[0]/2-x).argmin()+toll]):
                    state[j] = 'c'
                else:
                    state[j] = 'o'
            else:
                if E_T[j] <= max(U_eff_DT[np.abs((d_pl[0]/2+d_pl[1])-x).argmin()-toll:np.abs((d_pl[0]/2+d_pl[1])-x).argmin()+toll]):
                    state[j] = 'c'
                else:
                    state[j] = 'o'
        else: #(1,1,0) gapterno, to check!
            if ((x_values[j] >= 0) and (x_values[j] <= d_pl[0]/2)):
                if E_T[j] <= U_eff_DT[np.abs(-d_pl[0]/2-x).argmin()]:
                    state[j] = 'c'
                else:
                    state[j] = 'o'
            else:
                if E_T[j] <= U_eff_DT[np.abs(d_pl[0]/2-x).argmin()]:
                    state[j] = 'c'
                else:
                    state[j] = 'o'                
else:
    for j in range(len_df):
        i = np.abs(x - x_values[j]).argmin()
        E_T[j] = E_K[j] + U_eff_DT[i]
        toll = 50
        if E_T[j] <= max(U_eff_DT[np.abs(0 - x).argmin()-toll:np.abs(0 - x).argmin()+toll]):
            state[j] = 'c'
        else:
            state[j] = 'o'

final_df_txt['E_K'] = list(E_K)
final_df_txt['E_T'] = list(E_T)
final_df_txt['state'] = list(state)

print(final_df_txt)

# In[60]:


    # state_counts = df_state['state'].value_counts()
    # total_states = state_counts.sum()
    # print("N of tagged particles:", total_states)
    # # Stampa il conteggio e la percentuale per ogni stato
    # print("Counts and percentages of each particle state:")
    # for state, count in state_counts.items():
    #     percentage = (count / total_states) * 100
    #     print(f"{state}: {count} ({percentage:.2f}%)")


    # # print(df_state['state'].value_counts())

    # # Creazione del dizionario dei risultati con tutti gli stati possibili
    # all_states = ['ch', 'dech', 'ob', 'co+ch', 'co+de', 'rech1', 'rech1+dech', 'rech2', 'rech2+dech', 'rech3', 'rech3+dech', 'rech4', 'rech4+dech', 'rech5', 'rech5+dech', 'rech6', 'rech6+dech', 'rech7', 'rech7+dech', 'rech8', 'rech8+dech', 'rech9', 'rech9+dech', 'rech10', 'rech10+dech', 'rech11', 'rech11+dech', 'rech12', 'rech12+dech', 'rech13', 'rech13+dech', 'rech14', 'rech14+dech', 'rech15', 'rech15+dech', 'rech16', 'rech16+dech', 'rech17', 'rech17+dech', 'rech18', 'rech18+dech', 'rech19', 'rech19+dech', 'rech20', 'rech20+dech']
    # state_counts = df_state['state'].value_counts()
    # results_dict = {state: int(state_counts.get(state, 0)) for state in all_states}

    # # Salvataggio del dizionario in un file JSON
    # with open(f'{path}/{BE}/tag_results_{BE}.json', 'w') as f:
    #     json.dump(results_dict, f)



fig, ax = plt.subplots(figsize=[9, 6])
j=42314
# Scatter plot di E_T vs x_v
ax.scatter(final_df_txt['x_v'].loc[final_df_txt["eventID"]==j], final_df_txt['E_T'].loc[final_df_txt["eventID"]==j], label='E_T', color='lime', s=5)
#ax.scatter(df['x_v'].loc[df["E_T"]<900], df["E_T"].loc[df["E_T"]<900], label='E_T', color='#00ff00', s=5)
ax.plot(x, U_eff_DT, label='U_eff_tot', color='red')  
ax.axhline(y=max(U_eff_DT[np.abs(0 - x).argmin()-50:np.abs(0 - x).argmin()+50]), color='blue', linestyle='--')
#ax.axhline(y=U_eff_DT[np.abs((d_pl[0] + d_pl[1])-x).argmin()], color='blue', linestyle='--')
ax.set_xlabel('x_v')
ax.set_xlim(0,d_pl[0]+d_pl[1])
ax.set_ylabel('E_T')
ax.legend()
ax.grid(True)
#plt.savefig(f"{path}/{BE}/plot_particles_potentials-{BE}.png", dpi=300)
plt.show()


# Crea una figura con subplot in una griglia 3x2
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=(15, 10))
j=42314
# Primo subplot
ax1.scatter(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['x_v'].loc[final_df_txt["eventID"]==j], color='blue', s=5)
ax1.set_xlabel('z[$\AA$]')
ax1.set_ylabel('x_v[$\AA$]')
ax1.grid(True)

# Secondo subplot
ax2.plot(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['xx'].loc[final_df_txt["eventID"]==j], color='#00ff00', linewidth=0.8)
ax2.set_xlabel('x_v')
ax2.set_ylabel('tx')
ax2.grid(True)

# Terzo subplot
ax3.scatter(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['tx'].loc[final_df_txt["eventID"]==j]*10**6, color='#1cdfc4', s=5)
ax3.set_xlabel('z[$\AA$]')
ax3.set_ylabel('tx[urad]')
ax3.grid(True)

# Quarto subplot
ax4.scatter(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['E_T'].loc[final_df_txt["eventID"]==j], color='#B200FF', s=5)
ax4.set_xlabel('z[$\AA$]')
ax4.axhline(y=max(U_eff_DT[np.abs(0 - x).argmin()-50:np.abs(0 - x).argmin()+50]), color='blue', linestyle='--')
ax4.set_ylabel('E_T [eV]')
ax4.grid(True)

# Quinto subplot
ax5.scatter(final_df_txt['z'].loc[final_df_txt["eventID"]==j], final_df_txt['E_K'].loc[final_df_txt["eventID"]==j], color='#7cf224', s=5)
ax5.set_xlabel('z[$\AA$]')
ax5.set_ylabel('E_K [eV]')
ax5.grid(True)

# Sesto subplot (aggiunto vuoto per completare la griglia)
ax6.axis('off')  # Disattiva il sesto subplot se non necessario

# Mostra il plot
plt.tight_layout()
#plt.savefig(f"{path}/{BE}/trajectories_and_other_stuff-{BE}.png", dpi=300)
plt.show()
        
# In[68]:
def classify_particles(df_part):
    """
    Function to classify particle trajectories with or without parallel calculation (by R. Negrello).
    It returns a dataframe with two columns [eventID', 'category']. 
    To avoid future disambiguations, I changed the second column name from 'state' to 'category' 
    and the returned dataframe name from 'df_state' to 'df_category'.
    In order to correctly use this function, include here (or before calling):
    from collections import Counter
    tag_counters = Counter()
    """
    
    from collections import Counter
    tag_counters = Counter()
    
    df_category = pd.DataFrame(columns=['eventID', 'category'])
    
    results = []

    for event_id, group in df_part.groupby('eventID'):
        states = group['state'].tolist()
    #while False:

        #print('eventID', event_id)
        
        states = group['state'].tolist()
        final_state = 'Unknown'
        
        # Counts state occurrence
        state_counts = Counter(states)
        
        # Channeling
        if state_counts['c'] == len(states) and states[0]=="c" and states[-1]=="c": 
            final_state = 'ch'
            
        # Overbarrier
        elif state_counts['o'] == len(states):
                final_state = 'ob'        
        else:

            if "o" in states and "c" in states:
                ob_index = states.index('o')
                ch_index = states.index('c')  

            # Captured Overbarrier
                if states[0]=="o": # and all(state == 'o' for state in states[:ch_index]):
                    if all(state == 'c' for state in states[ch_index:]):
                        final_state = 'co+ch'
                    elif all(state in ['c', 'o'] for state in states[ch_index:]):
                        final_state = 'co+de'
                
            # Dechanneling 
                elif (states[0]=="c") and (states[-1]=="o") and all(state=='c' for state in states[:ob_index]) \
                                      and all(state=='o' for state in states[ob_index:]):
                        final_state = 'dech'
                
                # Rechanneling
                else:
                    ch_indexes = [i for i, state in enumerate(states) if state == 'c']
                    num_rech_transitions = 0
                    
                    # check transitions from "ch" to "non-ch" states and then again to "ch"
                    for i in range(len(ch_indexes) - 1):
                        start_index = ch_indexes[i]
                        end_index = ch_indexes[i+1]
                        if any(state != 'c' for state in states[start_index+1:end_index]) and states[end_index] == 'c':
                            num_rech_transitions += 1
                            
                    if num_rech_transitions == 1:
                        if states[-1]=="c":
                            final_state = 'rech1'
                        else:
                            final_state = 'rech1+dech'
                    elif num_rech_transitions == 2:
                        if states[-1]=="c":
                            final_state = 'rech2'
                        else:
                            final_state = 'rech2+dech'
                    elif num_rech_transitions == 3:
                        if states[-1]=="c":
                            final_state = 'rech3'
                        else:
                            final_state = 'rech3+dech'
                    elif num_rech_transitions == 4 :
                        if states[-1]=="c":
                            final_state = 'rech4'
                        else:
                            final_state = 'rech4+dech'
                    elif num_rech_transitions == 5 :
                        if states[-1]=="c":
                            final_state = 'rech5'
                        else:
                            final_state = 'rech5+dech'
                    elif num_rech_transitions == 6 :
                        if states[-1]=="c":
                            final_state = 'rech6'
                        else:
                            final_state = 'rech6+dech'
                    elif num_rech_transitions == 7:
                        if states[-1]=="c":
                            final_state = 'rech7'
                        else:
                            final_state = 'rech7+dech'
                    elif num_rech_transitions == 8:
                        if states[-1]=="c":
                            final_state = 'rech8'
                        else:
                            final_state = 'rech8+dech'
                    elif num_rech_transitions == 9 :
                        if states[-1]=="c":
                            final_state = 'rech9'
                        else:
                            final_state = 'rech9+dech'
                    elif num_rech_transitions == 10 :
                        if states[-1]=="c":
                            final_state = 'rech10'
                        else:
                            final_state = 'rech10+dech'
                    elif num_rech_transitions == 11 :
                        if states[-1]=="c":
                            final_state = 'rech11'
                        else:
                            final_state = 'rech11+dech'
                    elif num_rech_transitions == 12:
                        if states[-1]=="c":
                            final_state = 'rech12'
                        else:
                            final_state = 'rech12+dech'
                    elif num_rech_transitions == 13:
                        if states[-1]=="c":
                            final_state = 'rech13'
                        else:
                            final_state = 'rech13+dech'
                    elif num_rech_transitions == 14 :
                        if states[-1]=="c":
                            final_state = 'rech14'
                        else:
                            final_state = 'rech14+dech'
                    elif num_rech_transitions == 15 :
                        if states[-1]=="c":
                            final_state = 'rech15'
                        else:
                            final_state = 'rech15+dech'
                    elif num_rech_transitions == 16 :
                        if states[-1]=="c":
                            final_state = 'rech16'
                        else:
                            final_state = 'rech16+dech'
                    elif num_rech_transitions == 17:
                        if states[-1]=="c":
                            final_state = 'rech17'
                        else:
                            final_state = 'rech17+dech'
                    elif num_rech_transitions == 18:
                        if states[-1]=="c":
                            final_state = 'rech18'
                        else:
                            final_state = 'rech18+dech'
                    elif num_rech_transitions == 19 :
                        if states[-1]=="c":
                            final_state = 'rech19'
                        else:
                            final_state = 'rech19+dech'
                    elif num_rech_transitions == 20 :
                        if states[-1]=="c":
                            final_state = 'rech20'
                        else:
                            final_state = 'rech20+dech'
        results.append({'eventID': event_id, 'category': final_state})

    df_category = pd.DataFrame(results)
    return df_category



#print(df_txt_filtered[df_txt_filtered['eventID']==16722]['state'].tolist())
df_category = classify_particles(final_df_txt)
df_rad_filtered = df_rad_filtered.drop(['spectra'], axis=1)
print('df_category\n',df_category)

category_groups = df_category.groupby('category')['eventID'].apply(list).reset_index()
print('df_rad_filtered\n', df_rad_filtered)
print(category_groups)

df_cat_and_rad  = pd.merge(df_rad_filtered, df_category, on='eventID', how='left')
print(df_cat_and_rad)
#print(df_rad_filtered[df_rad_filtered['eventID']==60456])
categories_lst = df_category['category'].unique().tolist()

fig, axs = plt.subplots(len(categories_lst))
fig.set_size_inches(10, 5)
for ax, category in zip(axs, categories_lst):
    print(str(category))
    ids_cat=df_category[df_category['category']==str(category)]['eventID'].tolist()
    print(ids_cat)

    #print(df_category[str(category)])
    E_lst_cat = df_rad_filtered[df_rad_filtered['eventID'].isin(ids_cat)]['E_bc']
    hist, bin_edges = np.histogram(E_lst_cat, bins=10)
    bin_centers = 0.5 * (bin_edges[:-1] + bin_edges[1:])

    ax.plot(bin_centers, hist, marker='.')
    ax.set_title(str(category))
    ax.set_xlabel('Energy, MeV')
    ax.set_ylabel('Spectra, $\dfrac{\Delta N}{\Delta(\hbar\omega)}$')

plt.tight_layout()
plt.show()

    #hist, bin_edges = np.histogram(df_rad_filtered['E_bc'].loc[ids_cat])



    # #logi = (df_merged['angle_x'] > -1500e-6) & (df_merged['angle_x'] < 1000e-6)
    # df_filtered = df_merged

    # categories = df_filtered['state'].unique()
    # masks = {cat: df_filtered['state'] == cat for cat in categories}

    # # Definisci range e bins per l'istogramma
    # ranges = [-1000e-6, 1500e-6]
    # nbin = 300

    # # Prepara la figura per il plot
    # fig, ax = plt.subplots(figsize=(10, 6))
    # hist_data = {}
    # counts_all, bins_all = np.histogram(df_filtered['angle_x'], bins=nbin, range=ranges)
    # ax.hist(bins_all[:-1], bins=bins_all, weights=counts_all, alpha=0.7, label='all')
    # hist_data['all'] = counts_all
    # # Plotta gli istogrammi per tutte le categorie
    # for cat, mask in masks.items():
    #     counts, bins = np.histogram(df_filtered['angle_x'][mask], bins=nbin, range=ranges)
    #     ax.hist(bins[:-1], bins=bins, weights=counts, alpha=0.7, label=cat)  # Uso i bordi dei bin e i conteggi
    #     hist_data[cat] = counts  # Salva i conteggi per ogni categoria

    # # Configura le etichette e il titolo del grafico
    # ax.set_xlabel('angle_x (radians)')
    # ax.set_ylabel('Frequency')
    # ax.set_title('Particle State Distribution')
    # ax.legend()
    # plt.tight_layout()

    # # Visualizza il grafico
    # plt.savefig(f"{path}/{BE}/fulltag-{BE}.png", dpi=300)

    # # Salva i conteggi dell'istogramma in un file CSV
    # hist_df = pd.DataFrame(hist_data, index=bins[:-1])
    # hist_df.to_csv(f'{path}/{BE}/histograms_data_{BE}.csv')

    # # In[51]:

    # df_t = pd.merge(df, df_state, on='eventID')
    # categories = ['dech', 'co+de', 'rech1', 'rech1+dech', 'rech2', 'rech2+dech', 'rech3', 'rech3+dech',
    #               'rech4', 'rech4+dech', 'rech5', 'rech5+dech', 'rech6', 'rech6+dech', 'rech7', 'rech7+dech',
    #               'rech8', 'rech8+dech', 'rech9', 'rech9+dech', 'rech10', 'rech10+dech']
    # filters = [df_t['state_y'] == cat for cat in categories]
    # combined_filter = filters[0]
    # for f in filters[1:]:
    #     combined_filter |= f  # Operatore OR per combinare i filtri

    # # Applicazione del filtro al DataFrame
    # df_dech = df_t[combined_filter]

    # df_xz = pd.DataFrame(columns=['eventID','x_entrance','x_dech', 'z_dech'])
    # df_grouped= df_dech.groupby('eventID')
    # for event_id, group in df_grouped:
    #     states = group['state_x'].tolist()
    #     #print(states)
    #     zj=group['z'].tolist()
    #     x_vj=group['x_v'].tolist()

    #     for i in range(1, len(states)):
    #         if (states[i-1]=='c') & (states[i]!='c'):
    #             dech_pos_z=(zj[i]+zj[i-1])/2
    #             dech_pos_x=(x_vj[i]+x_vj[i-1])/2
    #     df_xz = pd.concat([df_xz, pd.DataFrame({'eventID': [event_id], 'z_dech': [dech_pos_z], 'x_dech': [dech_pos_x], 'x_entrance': [x_vj[0]]})], ignore_index=True)

    # df_xz.to_parquet(f"{path}/{BE}/df_xz_{BE}.parquet", compression='gzip')
    # #print(df_xz)
    # df_combined = pd.merge(df1, df_t, on='eventID')

    # # Salvataggio del dataframe unito in formato Parquet con compressione massima
    # file_path = f"{path}/{BE}/all_data_combined_{BE}.parquet"
    # df_combined.to_parquet(file_path, compression='gzip')

    # # In[51]:

    # import matplotlib.colors as mcolors

    # x_dech = df_xz['x_dech']
    # z_dech = df_xz['z_dech']*10**(-4)
    # x_entrance = df_xz['x_entrance']

    # fig, ax = plt.subplots(figsize=(8, 6))
    # # Grafico a destra
    # num_bins_z = 90
    # num_bins_x = 200

    # use_log_scale = True 
    # matrix, z_edges, x_edges = np.histogram2d(z_dech, x_dech, bins=(num_bins_z, num_bins_x))
    # img = ax.imshow(matrix.T, cmap='jet', interpolation="None", aspect='auto', 
    #                  extent=[ z_edges[0], z_edges[-1], x_edges[-1], x_edges[0]])
    # if use_log_scale:
    #     img.set_norm(mcolors.LogNorm())

    # plt.colorbar(img, ax=ax, label='Frequency')
    # ax.set_xlabel('Crystal depth [$\mu$m]')
    # ax.set_ylabel('$x_{dechanneling}$ [$\AA$]')
    # ax.invert_yaxis()
    # ax.set_ylim(0, d_pl[0]+d_pl[1])
    # plt.show()
    # #plt.savefig(f"{path}/{BE}/x_dech_vs_z_dech_{BE}", dpi=300)

    # ########################################################################à
    # import matplotlib.colors as mcolors

    # x_dech = df_xz['x_dech']
    # z_dech = df_xz['z_dech']*10**(-4)
    # x_entrance = df_xz['x_entrance']

    # fig, ax = plt.subplots(figsize=(8, 6))
    # # Grafico a destra
    # num_bins_z = 90
    # num_bins_x = 200

    # use_log_scale = True 
    # matrix, z_edges, x_edges = np.histogram2d(z_dech, x_entrance, bins=(num_bins_z, num_bins_x))
    # img = ax.imshow(matrix.T, cmap='jet', interpolation="None", aspect='auto', 
    #                  extent=[ z_edges[0], z_edges[-1], x_edges[-1], x_edges[0]])
    # if use_log_scale:
    #     img.set_norm(mcolors.LogNorm())

    # plt.colorbar(img, ax=ax, label='Frequency')
    # ax.set_xlabel('Crystal depth [$\mu$m]')
    # ax.set_ylabel('$x_{dechanneling}$ [$\AA$]')
    # ax.invert_yaxis()
    # ax.set_ylim(0, d_pl[0]+d_pl[1])
    # plt.show()
    # plt.savefig(f"{path}/{BE}/x_entr_vs_z_dech_{BE}", dpi=300)
    # ################################################################
    # from mpl_toolkits.axes_grid1 import make_axes_locatable
    # fig, ax = plt.subplots(figsize=(10, 6))
    # hist_matrix, z_edges, x_edges = np.histogram2d(z_dech, x_entrance, bins=(num_bins_z, num_bins_x))
    # img = ax.imshow(hist_matrix.T, cmap='jet', interpolation="None", aspect='auto', origin='lower',
    #                 extent=[z_edges[0], z_edges[-1], x_edges[0], x_edges[-1]])
    # if use_log_scale:
    #     img.set_norm(mcolors.LogNorm())
    # plt.colorbar(img, ax=ax, label='Frequency')
    # ax.set_xlabel('Crystal depth [$\mu$m]')
    # ax.set_ylabel('$x_{entrance}$ [$\AA$]')
    # ax.invert_yaxis()
    # ax.set_ylim(0, d_pl[0] + d_pl[1])

    # # Aggiunta della proiezione sommata su y
    # projection_y = np.sum(hist_matrix, axis=0)  # Somma lungo le colonne per la proiezione su y
    # x_projection = (x_edges[:-1] + x_edges[1:]) / 2  # Calcolo del centro dei bin

    # # Creazione di un nuovo assi per il grafico a barre sovrapposto
    # divider = make_axes_locatable(ax)
    # cax = divider.append_axes("right", size="25%", pad=0.05)
    # cax.barh(x_projection, projection_y, height=np.diff(x_edges)[0], color='gray', alpha=0.7)
    # cax.set_ylim(ax.get_ylim())  # Allinea i limiti y con l'asse principale
    # cax.set_xlabel('Counts Sum')
    # cax.set_yticks([])
    # #cax.invert_yaxis()

    # df_filtered = df_t[df_t['state_y'] == 'ch']
    # df_filtered = df_filtered[df_filtered.groupby('eventID')['xx'].transform('first').between(1.0, 2.0)]

    # # Seleziona solo le prime 10 particelle uniche per semplicità
    # unique_events = df_filtered['eventID'].unique()[5:10]
    # df_plot = df_filtered[df_filtered['eventID'].isin(unique_events)]
    # for event_id in unique_events:
    #     data = df_plot[df_plot['eventID'] == event_id]
    #     ax.plot(data['z']*1e-4, data['xx'], label=f'Event {event_id}',color='orange')

    # plt.savefig(f"{path}/{BE}/x_impact_vs_z_dech_{BE}_wtraj", dpi=300)
    # # Mostra il plot
    # plt.show() 

    # # %%
    # from mpl_toolkits.axes_grid1 import make_axes_locatable
    # fig, ax = plt.subplots(figsize=(10, 6))
    # hist_matrix, z_edges, x_edges = np.histogram2d(z_dech, x_entrance, bins=(num_bins_z, num_bins_x))
    # img = ax.imshow(hist_matrix.T, cmap='jet', interpolation="None", aspect='auto', origin='lower',
    #                 extent=[z_edges[0], z_edges[-1], x_edges[0], x_edges[-1]])
    # if use_log_scale:
    #     img.set_norm(mcolors.LogNorm())
    # plt.colorbar(img, ax=ax, label='Frequency')
    # ax.set_xlabel('Crystal depth [$\mu$m]')
    # ax.set_ylabel('$x_{entrance}$ [$\AA$]')
    # ax.invert_yaxis()
    # ax.set_ylim(0, d_pl[0] + d_pl[1])

    # # Aggiunta della proiezione sommata su y
    # projection_y = np.sum(hist_matrix, axis=0)  # Somma lungo le colonne per la proiezione su y
    # x_projection = (x_edges[:-1] + x_edges[1:]) / 2  # Calcolo del centro dei bin

    # # Creazione di un nuovo assi per il grafico a barre sovrapposto
    # divider = make_axes_locatable(ax)
    # cax = divider.append_axes("right", size="25%", pad=0.05)
    # cax.barh(x_projection, projection_y, height=np.diff(x_edges)[0], color='gray', alpha=0.7)
    # cax.set_ylim(ax.get_ylim())  # Allinea i limiti y con l'asse principale
    # cax.set_xlabel('Counts Sum')
    # cax.set_yticks([])
    # #cax.invert_yaxis()

    # df_filtered = df_t[df_t['state_y'] == 'ch']
    # df_filtered = df_filtered[df_filtered.groupby('eventID')['xx'].transform('first').between(1.0, 2.0)]

    # # Seleziona solo le prime 10 particelle uniche per semplicità
    # unique_events = df_filtered['eventID'].unique()[5:10]
    # df_plot = df_filtered[df_filtered['eventID'].isin(unique_events)]
    # for event_id in unique_events:
    #     data = df_plot[df_plot['eventID'] == event_id]
    #     #ax.plot(data['z']*1e-4, data['xx'], label=f'Event {event_id}',color='orange')

    # plt.savefig(f"{path}/{BE}/x_impact_vs_z_dech_{BE}_notraj", dpi=300)
    # # Mostra il plot
    # #plt.show() 
# %%
