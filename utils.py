import os, uproot, pandas as pd, numpy as np, multiprocessing
from collections import Counter


        
def parallelize_dataframe_parts(df_parts, func, *args):
    num_processes = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(num_processes)
    results = pool.starmap(func, [(df_part, *args) for df_part in df_parts])
    pool.close()
    pool.join()
    return results

def calculate_E_K(df_part, BeamEnergy):
    E_K_partial = []
    for _, row in df_part.iterrows():
        E_K = BeamEnergy / 2 * row['tx'] ** 2
        E_K_partial.append(E_K)
    return E_K_partial

def calculate_E_T(df_part, x, BeamEnergy, U_eff):
    E_T_partial = []
    for _, row in df_part.iterrows():
        i = np.abs(x - row['x_v']).argmin()
        E_T = BeamEnergy / 2 * row['tx'] ** 2 + U_eff[i]
        E_T_partial.append(E_T)
    return E_T_partial


def determine_states(df_part, x, particle, BeamEnergy, U_eff, d_pl):
    if particle == 'e+': #Positrons
        states_partial = []
        for _, row in df_part.iterrows():
            i = np.abs(x - row['x_v']).argmin()
            E_T = BeamEnergy/2 * row['tx']**2 + U_eff[i]
            if ((row['x_v'] >= 0) and (row['x_v'] <= (d_pl[0] / 2))):
                if E_T <= max(U_eff[np.abs(-(d_pl[0] / 2) - x).argmin()-150:np.abs(-(d_pl[0] / 2) - x).argmin()+150]):
                    state = 'c'
                else:
                    state = 'o'
            elif((row['x_v']>= d_pl[0]/2) and (row['x_v']<=d_pl[0]/2+d_pl[1])):
                if E_T <= max(U_eff[np.abs(d_pl[0] / 2 - x).argmin()-150:np.abs(d_pl[0] / 2 - x).argmin()+150]):
                    state = 'c'
                else:
                    state = 'o'
            else:
                if E_T <= max(U_eff[np.abs((d_pl[0]/2+d_pl[1])-x).argmin()-150:np.abs((d_pl[0]/2+d_pl[1])-x).argmin()+150]):
                    state = 'c'
                else:
                    state = 'o'
            states_partial.append(state)
        return states_partial
    if particle =='e-': #electrons
        states_partial = []
        for _, row in df_part.iterrows():
            i = np.abs(x - row['x_v']).argmin()
            E_T = BeamEnergy/2 * row['tx']**2 + U_eff[i]
            if E_T <= max(U_eff[np.abs(0 - x).argmin()-50:np.abs(0 - x).argmin()+50]):
                state = 'c'
            else:
                state = 'o'
            states_partial.append(state)
        return states_partial
    

def get_defl_data(defl_path):
        dfs_list = []
        for filename in os.listdir(defl_path):
            ID= int(filename.split('.')[0])
            file_path = os.path.join(defl_path, filename)
            if os.path.isfile(file_path):
                file = uproot.open(file_path)
                tree = file["scoring_ntuple;1"]
                #print(tree.keys())
                df_tree = tree.arrays(['volume', 'x', 'y', 'angle_x', 'angle_y'], library='pd')
                df_tree['eventID'] = [ID] * len(df_tree['volume'])
                df_tree = df_tree.loc[df_tree['volume'] == 'Detector']
                dfs_list.append(df_tree)
        
        return pd.concat(dfs_list, ignore_index=True)

def get_rad_data(radfilespath, apply_collimation, coll_angle):

    dataframes = []
    for filename in os.listdir(radfilespath):
        #print(filename.split())
        ID= int(filename.split('.')[0])
        file_path = os.path.join(radfilespath, filename)
        if os.path.isfile(file_path):
            combined_thetaX = []
            combined_thetaY = []
            combined_Evalues = []
            rf = uproot.open(file_path)
            rf_content = [item.split(';')[0] for item in rf.keys()]

            if 'photon_spectrum' in rf_content:
                df_ph = rf['photon_spectrum'].arrays(library='pd')
                Evalues = df_ph["E"].values
                #print("Number of photons scored in", filename, ":", Evalues.shape[0])
                if Evalues.shape[0]==0: 
                    #print('No photons emitted')
                    continue
                
                thetaX, thetaY = df_ph["angle_x"].values, df_ph["angle_y"].values
                theta = np.sqrt(thetaX**2 + thetaY**2)  

                if apply_collimation: 
                    thetaX = thetaX[(theta <= coll_angle) & (theta <= coll_angle)]
                    thetaY = thetaY[(theta <= coll_angle) & (theta <= coll_angle)]
                    Evalues = Evalues[theta <= coll_angle]
                    theta = theta[theta <= coll_angle]

                # Append
                combined_thetaX.extend(thetaX)
                combined_thetaY.extend(thetaY)
                combined_Evalues.extend(Evalues)

            sim_spectrum, Ebin = np.histogram(combined_Evalues, bins=50)
            E_center = (Ebin[:-1] + Ebin[1:]) / 2
            EventID = [ID] * len(sim_spectrum)
            sim_spectral_intensity = E_center * sim_spectrum
            DE = Ebin[1] - Ebin[0]
            print('Bin dimension:', round(DE, 3), 'MeV')
            df = pd.DataFrame({
                "eventID": EventID,
                "E_bc": E_center,
                "spectra": sim_spectrum,
                #"spectral intensity" : sim_spectral_intensity
            })
            dataframes.append(df)
    df_rad = pd.concat(dataframes)    

    return df_rad



def open_txt_file(txtpath):
    dataframes = []
    for filename in os.listdir(txtpath):
        # Initialize the list inside the for cycle since I have only one particle per simulation
        trackID = []  # saved but not useful
        x_v = []
        tx = []
        z = []
        xx = []
        
        ID = int(filename.split('.')[0])
        file_path = os.path.join(txtpath, filename)
        
        if os.path.isfile(file_path):
            with open(file_path, 'r') as file:
                for line in file:
                    values = line.strip().split()
                    trackID.append(int(values[1]))
                    x_v.append(float(values[2])*10**7)  # Converti in Angstrom
                    tx.append(float(values[3]))
                    z.append(float(values[4])*10**7)  # Converti in Angstrom
                    xx.append(float(values[5])*10**7)  # Converti in Angstrom
            
            
            df = pd.DataFrame({
                "eventID": [ID] * len(trackID),
                "x_v": x_v,
                "tx": tx,
                "z": z,
                "xx": xx
            })
            dataframes.append(df)
    
    df_txt = pd.concat(dataframes, ignore_index=True)   
    return df_txt




